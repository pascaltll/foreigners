### SQL over MapReduce. Hive

##### 1. Starting Hive shell
There are 3 primary ways to run a Hive job. Let's check them out using `SHOW DATABASES` (lists existing Hive databases).
* interactive shell:

    ``` bash
    $ hive
    hive> SHOW DATABASES;
    ```
*  external command:
    ``` bash
    $ hive -e 'SHOW DATABASES'
    ```
* external file:
    ``` bash
    $ echo 'SHOW DATABASES' > sh_db.sql # write to file
    hive -f sh_db.sql
    ```
Hive shell also allows running usual shell commands.
*No spaces after '__!__', '__;__' after the command*.
Get the number of virtual CPUs on client:
```bash
hive> !nproc;
```
Composite commands like `cat file | grep 'key' | tee new_file` are not supported.
You can also work with hdfs from inside hive.
```bash
hive> dfs -ls;
```

##### 1.1. HUE

* Port forwarding: `ssh <USERNAME>@mipt-client.atp-fivt.org -L 8888:mipt-node03.atp-fivt.org:8888`
* Username and password can be found in the chat.

##### 2. Creating a database
*
    ``` bash
    hive> create database <YOUR_USER>_test location '/user/<YOUR_USER>/test_metastore';
    ```
    `Hive metastore` is a relational DB located in HDFS that stores metadata for tables. While creating a DB, you must specify **full path** to the metastore.
* If you specified wrong name or location, you can delete the database:
    ``` bash
    hive> drop database if exists <YOUR_USER>_test cascade;
    ```
    Keyword `CASCADE` is responsible for deleting the database together with its contents.
* Get DB description
    ``` bash
    hive> DESCRIBE DATABASE <YOUR_USER>_test
    ```
Exit the hive shell.

##### 3. Creating a table
Let's create a table in our test database. Use the "subnets" dataset (`/data/subnets/variant1`) as sample data:

    * IP address,
    * subnet mask for this address

``` sql
ADD JAR /opt/cloudera/parcels/CDH/lib/hive/lib/hive-contrib.jar;
USE <YOUR_USER>_test;
DROP TABLE IF EXISTS Subnets;

CREATE EXTERNAL TABLE Subnets (
    ip STRING,
    mask STRING
)
ROW FORMAT DELIMITED FIELDS TERMINATED BY  '\t'
STORED AS TEXTFILE
LOCATION '/data/subnets/variant1';
```
Explanation
1. `ADD JAR` - add a  Jar containing Hive classes, for queries that should run MapReduce jobs.
2. `USE ...` - connect to a database. Without this command, tables will be created in a database called "default". Alternatively, you can specify the argument `--database` when running the query.
3. `EXTERNAL` - there are 2 types of tables: managed and external. External tables work with external data without modifying it; managed tables allow modification.
4. `STORED AS` - storage format. For external tables, this should coincide with the data storage format. For managed tables, formats that use compression are recommended (RCFile, AVRO etc).

Save the script to a file, save it and run:
``` bash
hive -f my_query.hql
```
Check out our table (print first 10 rows):
```bash
hive --database <YOUR_USER>_test -e 'SELECT * FROM Subnets LIMIT 10'
```

List tables in the database:

```bash
hive --database <YOUR_USER>_test -e 'SHOW TABLES'
```

##### 4. Partitioning
Let's create a partitioned table from "Subnets". Information about each partition is stored in a separate HDFS directory inside metastore.
``` sql
ADD JAR /opt/cloudera/parcels/CDH/lib/hive/lib/hive-contrib.jar;

SET hive.exec.dynamic.partition.mode=nonstrict;

USE <YOUR_USER>_test;
DROP TABLE IF EXISTS SubnetsPart;

CREATE EXTERNAL TABLE SubnetsPart (
    ip STRING
)
PARTITIONED BY (mask STRING)
STORED AS TEXTFILE;

INSERT OVERWRITE TABLE SubnetsPart PARTITION (mask)
SELECT * FROM Subnets;
```

You can already see that the query is converted into a  MapReduce job. To verify this, you can access the ApplicationMaster UI: http://mipt-master.atp-fivt.org:8088.
Find there a single MapReduce Job containing only the map stage.

`SET hive.exec.dynamic.partition.mode=nonstrict;` - enable dynamic partitioning. By default, Hive partitions a table statically, i.e. a fixed number of partitions are created. But in this case we don't know in advance how many unique mask values we have, and therefore do not know the required number of partitions.

Check out the resulting partitions:
```bash
hive --database <YOUR_USER>_test -e 'SHOW PARTITIONS SubnetsPart'
```

`SET` allows you to specify other job options. For example, set a name for all jobs that will be generated: `SET mapred.job.name=my_query;`. Another way is to pass an argument `--hiveconf hive.session.id=my_query`.

Sources: `/home/velkerr/seminars/mcs17_hive1`.

##### 5. Parsing data using regular expressions
1. Create a new table.

    ``` sql
    add jar /opt/cloudera/parcels/CDH/lib/hive/lib/hive-contrib.jar;
    add jar /opt/cloudera/parcels/CDH/lib/hive/lib/hive-serde.jar;
    USE <YOUR_USER>_test;
    DROP TABLE IF EXISTS SerDeExample;

    CREATE EXTERNAL TABLE SerDeExample (
        ip STRING,
        date STRING,
        request STRING,
        responseCode STRING
    )
    ROW FORMAT SERDE 'org.apache.hadoop.hive.contrib.serde2.RegexSerDe'
    WITH SERDEPROPERTIES (
        "input.regex" = '^(\\S*)\\t.*$'
    )
    STORED AS TEXTFILE
    LOCATION '/data/user_logs/user_logs_S';

    select * from SerDeExample limit 10;
    ```
    Output:
    ```
    135.124.143.193	NULL	NULL	NULL
    247.182.249.253	NULL	NULL	NULL
    135.124.143.193	NULL	NULL	NULL
    222.131.187.37	NULL	NULL	NULL
     ...
    ```

    Groups in the regex are matched to fields. By default, matching is done in the same order as the order of groups in the expression. To change this, use `"output.format.string"`. [more](https://cwiki.apache.org/confluence/display/Hive/UserGuide).

    We've managed to separate the first field. The rest are now NULLs.
2. Let's try the next field. Modify the regex (3 tabs here are an exception, the other fields are separated with a single tab).

    ```
    "input.regex" = '^(\\S*)\\t\\t\\t(\\S*)\\t.*$'
    ```
    Save, run: `$ hive –f myQuery.sql`
    Output:

    ```
    135.124.143.193	20150601013300	NULL	NULL
    247.182.249.253	20150601013354	NULL	NULL
    135.124.143.193	20150601013818	NULL	NULL
    222.131.187.37	20150601013957	NULL	NULL
     ...
    ```

A useful tool for debugging regular expressions: https://regex101.com/ (replace `//` with `/`).

![Example](https://i.stack.imgur.com/jvAzy.png)

3. To parse the data correctly, use the following regex:
```
"input.regex" = '^(\\S*)\\t{3}(\\d{8})\\S*\\t(\\S*)\\t\\S+\\t(\\S*)\\t.*$'
```
A stronger version (`\S+` or `\d+` instead of `\S*` allows us to reject incorrect strings).
```
"input.regex" = '^(\\S+)\\t{3}(\\d{8})\\d+\\t(\\S+)\\t\\d+\\t(\\d+)\\t.*$
```
> **Exercise.** Add this regex to the query and verify that the data has been parsed correctly.

***Note.*** `SerDe` class from `hive.contrib` имеет одну особенность. The output always consists of strings, while there can also be numbers in the dataset. In that case, using another class called `org.apache.hadoop.hive.serde2.RegexSerDe` is recommended. This implementation of `SerDe` correctly parses the majority of Hive data types, but only supports deserialization (i.e. reading). The syntax for this version is almost the same, you only have to specify another class path for `ROW FORMAT SERDE`.

View table info:
``` bash
hive -S --database <YOUR_USER>_test -e 'DESCRIBE SerDeExample'
```
`-S` disables logging, execution timestamps etc. Only the result itself is left.
```
ip                  	string              	from deserializer   
date                	string              	from deserializer   
request             	string              	from deserializer   
responsecode        	string              	from deserializer
```
##### 6. Practice
> **Problem 0.** Count distinct subnet masks.

Solution.
```
ADD JAR /opt/cloudera/parcels/CDH/lib/hive/lib/hive-contrib.jar;
USE <YOUR_USER>_test;

SELECT COUNT(DISTINCT mask)
FROM Subnets;
```
The `EXPLAIN` command shows the execution plan for a query, including the list of MapReduce jobs into which the query is converted.

```
STAGE DEPENDENCIES:
  Stage-1 is a root stage
  Stage-0 depends on stages: Stage-1

STAGE PLANS:
  Stage: Stage-1
    Map Reduce
      Map Operator Tree:
          TableScan
            ...
      Reduce Operator Tree:
        Group By Operator
          ...

  Stage: Stage-0
    Fetch Operator
      ...
```
Check it out on ApplicationMaster Web UI http://mipt-master.atp-fivt.org:8088.
> **Problem 1.** Count the addresses with mask 255.255.255.128.

We'll require filtering, so the partitioning done earlier should affect the execution speed.
* Run the query on Subnets (unpartitioned) and SubnetsPart (partitioned); compare what happens.
* View data size: `hdfs dfs -du -h /data/subnets/variant1`
* Re-create Subnets and SubnetsPart using dataset `/data/subnets/big` (7 Gb) and repeat the experiment. Has the difference increased?

> **Problem 2.** Count the average number of addresses in subnets.

> For each problem, print the execution plan and count the number of MR jobs.

##### 7. Hive streaming
Two primary ways to run external scripts in Hive Streaming:
* write a command directly
* use a script saved elsewhere

> **Example.** Print the first octet of each IP address in the Subnets table.

Two versions of solution: `/home/velkerr/seminars/mcs17_hive1/6-1-streaming_example`

 - `TRANSFORM`: select a field for processing
 - `USING`: command or script responsible for processing
 - `AS`: alias for resulting fields; several fields are also allowed

Don't forget to add external scripts to Distributed Cache with `ADD FILE` (like in Hadoop Streaming).

###### 7.1. Debugging Streaming scripts
External scripts can be quite complicated, so the ability to debug them would be useful.
``` bash
hive --database <YOUR_USER>_test -e 'SELECT * FROM SerDeExample LIMIT 10' | ./<your_script>.sh
```

###### 7.2. Practice
Input data: logs (SerDeExample).
> **Problem 4.** Change the date 20150601 in logs to current date using Streaming.

Input data: logs (SerDeExample) or subnets.
> **Problem 5.** Convert IP addresses to numerical representation. For quick conversion, this python code might be helpful: `struct.unpack("!I", socket.inet_aton(ip)`

> For each problem, again, print the execution plan and count the number of MR jobs.
