### Structure of a MapReduce job

```
public void run(Context context) {
    setup();
    while (keys.next()) {
        reduce(key, value);
    cleanup();
```

Here's an example for WordCount.
```
current_key = None
sum_count = 0
for line in sys.stdin:
    try:
        key, count = line.strip().split('\t', 1)
        count = int(count)
    except ValueError as e:
        continue
    if current_key != key:
        if current_key:
            print "%s\t%d" % (current_key, sum_count)
        sum_count = 0
        current_key = key
    sum_count += count
if current_key:
    print "%s\t%d" % (current_key, sum_count)
```

Identify setup(), cleanup(), run() stages here.

# Hadoop Java API

Source: `seminars/pd2020/05_wordcount_java`.

#### Main class

`public class WordCount extends Configured implements Tool {...}`

* Class WordCount containts all the logic of the job
* Base class [Configured](https://hadoop.apache.org/docs/r2.4.1/api/org/apache/hadoop/conf/Configured.html) is responsible for getting HDFS configuration (just call `getConf()` inside of `run()`). That's useful when you need to interact with HDFS from your code (for example, to remove intermediate data),
* [Tool](https://hadoop.apache.org/docs/stable/api/org/apache/hadoop/util/Tool.html) - an interface containing a single method `run()` that (a) parses the command line arguments (b) configures the job. `run()` is executed **on client**.

#### Method `run()`: important details

1. I/O formats:
```java
job1.setInputFormatClass(TextInputFormat.class);
job1.setOutputFormatClass(TextOutputFormat.class);
```
These are formats in which the job's result is written. Hadoop has several built-in i/o formats. Primary ones:

**TextInputFormat / TextOutputFormat**
Results are written to a standard text file.
Pros: fast.
Cons: erases information about data types of keys and values. When reading, we'll have pairs `(LongWritable offset, Text rawString)`, where `rawString` should be parsed.

**KeyValueTextInputFormat / KeyValueTextOutputFormat**
An improved version. Stores the types, but only if those are primitive (IntWritable, Text).

**SequenceFileInputFormat / SequenceFileTextOutputFormat**
Pros: stores data types.
Cons: the output file will be binary, and you won't be able to read it with `hdfs dfs -cat`.

Conclusion:
* In intermediate jobs, it's better to use KeyValue or SequenceFile.
* In final job - TextOutputFormat

Any I/O format allows to enable data compression and therefore optimize network transfer.

```java
SequenceFileOutputFormat.setOutputCompressionType(job, CompressionType.BLOCK);
SequenceFileOutputFormat.setCompressOutput(job, true);
```

`CompressionType` can be either `BLOCK` (compresses groups of records) or `RECORD` (compresses individual blocks).

[More](http://timepasstechies.com/input-formats-output-formats-hadoop-mapreduce/) I/O formats.

2. Setting the number of mappers and reducers.

`job.setNumReduceTasks(8);` - set the number of reducers.
The number of mappers can't be specified directly. The system sets it equal to the number of splits.
* Split size can be configured (equal to block size by default)
* In the log of a Hadoop task you can find a line like this: `19/10/14 21:37:44 INFO mapreduce.JobSubmitter: number of splits:2`.

#### Mappers and reducers

Follow the pattern above. Unlike Hadoop Streaming, you don't need to iterate splits.
Executed on nodes.

#### Wrapper classes
There are two important interfaces in Hadoop, `Writable` и `WritableComparable`. They both support serialization / deserialization needed to pass data between nodes.
* Key types must implement `WritableComparable`, since they will be compared in the sort phase.
* For value types, `Writable` is enough.
* Wrapper classes in Hadoop distribution implement `WritableComparable`.

#### Build and run
A Java project can be built using ant or maven. See build.xml and pom.xml in `/home/velkerr/seminars/pd2020/05_wordcount_java` respectively.
Running a program: `hadoop jar <path_to_jar> <fully_qualified_name_of_main_class> <input> <output> [other_arguments (for isntance, number of reducers)]`

### Problem
1. Let's add a combiner to WordCount. In job settings, (`run()`) add `setCombinerClass`. Could a reducer also play a role of a combiner? Let's compare the execution time with and without combiner.
2. Modify WordCount:
   * do not count words shorter than 4 symbols (see "stop words").
   * Print only words with > 4 occurrences.

## Total Order Sorting
The output of a single reducer is sorted. How could we perform a global sorting among all reducers?
* Straightforward solution: iterate over all keys and explicitly tell the partitioner which keys should be sent to every reducer. That's too inefficient.
* Sampling. Instead of iterating over all keys, select a part of them probabilistically, then approximate over entire dataset.

Example: `InputSampler.Sampler<LongWritable, Text> sampler = new InputSampler.RandomSampler<>(0.5, 10000, 10);`.
Arguments:
* 0.5 - probability of selecting a record:
* maximum number of selected records
* maximum number of splits.

When the constraint for any single argument is reached, stop sampling.

Sampler is passed to TotalOrderPartitioner. See "Hadoop. The definitive guide" 4ed p. 287".
WordCount example with global order sorting: `/home/velkerr/seminars/pd2020/10-globalsort`.

### Problem
Let's experiment with InputSampler parameters. For example, decrease the probability to 0.2. If there isn't enough data, Sampler will not be able to form a sample and our code will fail.

### Joins
Determine the number of StackOverflow users who've asked questions with FavoriteCount >= 100 and given correct answers, i.e. with maximum Score for a questions. Hadoop Counters might help you.

Input: posts on stackoverflow.com
Output format: user ids, one per line

Print the number of users.

In the next class, we'll write joins like that in several lines on HiveQL.

Note the classes `StackExchangeEntry` and `PairWritable` as examples for writing your own Writable wrappers.


### More Joins
On StackOverflow data, count the bar chart for questions and answers depending on user age. If age is absent or invalid, consider it equal to 0. Print the chart, sorted numerically by age.

* *Input:* stackoverflow.com posts
* *Output:* age <tab> num_questions <tab> num_answers

##### Input data description

`/data/stackexchange/posts` - posts written in lines beginning with ‘<row’ (you can parse them manually). Fields:

* PostTypeId - ‘1’ if question, ‘2’ if answer
* Id; if a question, can be opened like this http://stackoverflow.com/questions/<Id>
* Score - measure of usefulness of this question or answer
* FavoriteCount - how many times the question was added to favorites by someone
* ParentId - if an answer, the question's id
* **OwnerUserId - author's id**

`/data/stackexchange/users` - users

* **Id - user id** (OwnerUserId in posts)
* Age (may be absent)
* Reputation (for a user)

There are several samples of this dataset (stackexchange100 and stackexchange1000 represent a 100th and a 1000th of the original dataset respectively).

##### Reduce-side join

<img src="images/reduce_side_join.png"  width="500" height="360">

##### Solution scheme:
*1st job*
* **Mapper**: line of dataset (users or posts) -> pairs of these types:
    * (userId, tag, age, 0, 0) for user,
    * (userId, tag, 0, 1, 0) for question
    * (userId, tag, 0, 0, 1) for answer

`tag` distinguishes between users and posts (for example, `U` for users and `P` for posts).

`userId` - primary key on which we will join.

`age` should be stored for sorting on it

It's harder to work with composite keys. In Hadoop Streaming, you should configure comparator and partitioner, in Java API it's more convenient to define your own Writable class (with serialization). In our example, that's StackExchangeEntry.java

* **Reducer**: (userId, [(tag, age, 0, 0), (tag, 0, 1, 0), (tag, 0, 1, 0), ...]) -> (age, num of questions, num of answers)

*2nd job*
* Aggregate tuples (age, num of questions, num of answers). WordCount by age (key: age, value: q's and a's).
* Sort by age.

Solution source: `/home/velkerr/seminars/pd2020/11-joins`.

**Difficult**. Use Hive.

### Another problem on MapReduce
Estimate mean lifetime of a domain's pages in days. Sort by lifetime. Results: `echo.msk.ru 5` means that pages on "Echo of Moscow" website are visited in a span of 5 days on average. Significance of this metric: on news websites, new pages are added frequently; materials on websites of glossy, automobile, scientific websites are usually visited for several weeks. We want to test the hypothesis that page lifetime on these resources will differ.

We're given a dataset of visits of different pages on the net, containing visit timestamp and durations of visits, collected over 2 weeks. Format: tab-separated, fields: user_id, timestamp, url, 'diff_time':time_on_page.

Example:
```
7884862957065236246 1412508741 http://lenta.ru/articles/2014/10/05/teacher/ 63
```

Explanation:

* 7884862957065236246 - depersonalized user id,
* 1412508741 - unix timestamp,
* page address and time spent on page in seconds
* diff_time: - useless for us.

The data is located at path `/data/user_events` in HDFS, and there's also a sample for debugging at `/data/user_events_part`.

**Describe the computation scheme on MapReduce.**

Source: `/home/velkerr/seminars/pd2020/12-avg-page-life`.

# Hadoop Streaming advanced
Data: activity statistics of a Minecraft server's users
Sort by average number of chat commands. Break ties by comparing usernames lexicographically.
```
Login    average num of chat commands during session     total num of sessions
```
Path to data: `/data/minectaft_user_activity`

##### Output example:
```
lucky20    25.0    10
avivzusim   5.0    15
```

### Solution
1. Specify that we have 3 fields
2. Use KeyFieldBasedComparator
3. Configure sorting by the second field in decreasing order (r) and numerical (n) sorting.
4. Break ties by 1st field.
See `man sort` for more.

```bash
#!/usr/bin/env bash

IN_DIR="/data/minectaft_user_activity"
OUT_DIR="minecraft_result"

# Look at the input data
hdfs dfs -cat ${IN_DIR}/part-00000 | head -n 10

# Remove previous results
hdfs dfs -rm -r -skipTrash ${OUT_DIR}* > /dev/null

yarn jar /opt/cloudera/parcels/CDH/lib/hadoop-mapreduce/hadoop-streaming.jar \
    -D stream.num.map.output.key.fields=3 \
    -D mapreduce.job.reduces=1 \
    -D mapreduce.job.output.key.comparator.class=org.apache.hadoop.mapreduce.lib.partition.KeyFieldBasedComparator \
    -D mapreduce.partition.keycomparator.options='-k2,2nr -k1' \
    -mapper cat \
    -reducer cat \
    -input ${IN_DIR} \
    -output ${OUT_DIR}

# Check result
hdfs dfs -cat ${OUT_DIR}/part-00000 | head -n 10
```
