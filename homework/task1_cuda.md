# Task 1. CUDA programming
You're to solve one of two problems. You can use either CUDA API or PyCUDA, **but not CUBLAS**. For experimenting (running the code with different parameters), you can use whatever tools you want.

Work server: `ssh <user>@lorien.atp-fivt.org`

<h3> Grading </h3>

|Level|Max grade|%|
|---|---|---|
|1|1,5 points|75%|
|2|2 points|100%|

<h3> Deadlines </h3>


| Soft deadline | Hard deadline (-50%) |
| ------------- | -------------------- |
| March 02, 23:59 | March 09, 23:59    |

You can fix issues within a month after the code review.

# Level 1. Calculating the parameters of a normal distribution

Let $`X`$ be a random variable with a standard normal distribution. Given parameters $`a`$, $`b`$, $`c`$ (read them from the input stream), let's define $`Y = aX^2 + bX + c`$. You should generate $`n = 2^{28}`$ realizations of $`Y`$.

### Implementation details
1. Variables `a`, `b` and `c` should be saved to constant memory (`__constant__`) -- compare the execution time with passing through function arguments.
2. Look into ILP (Instruction Level Parallelism): try to set number of blocks equal to 1 and then load values in a loop. You should choose the optimal number of blocks and number of iterations. For this part, plot the dependency of execution time from these settings.
3. Try implementing random number generation both on `host` and on `device`. Compare the execution times.

The average of your sample should be approximately equal to $`a + c`$.

You can represent comparison results as a list or a table:
<p>
<details>
<summary markdown="span"><h4> Example </h4> </summary>

#### Task 1
|Version|Execution time|
|---|---|
|Passing through arguments|0.xxx|
|Passing through constant memory|0.yyy|

</details>
</p>

### Useful materials
* [Random number generation on host](https://docs.nvidia.com/cuda/curand/host-api-overview.html#host-api-example)
* [Random number generation on device](https://docs.nvidia.com/cuda/curand/device-api-overview.html#device-api-example)
* [Copying to constant memory space](https://docs.nvidia.com/cuda/cuda-runtime-api/group__CUDART__MEMORY.html#group__CUDART__MEMORY_1g9bcf02b53644eee2bef9983d807084c7)

# Level 2. Matrix multiplication
You're given two matrices; calculate their product. You may consider that the dimensions of these matrices are multiples of block size.

### Implementation details
1. Use shared memory for counting the product for a block.
2. Measure the execution time and compare it to that of the [naïve implementation](https://github.com/akhtyamovpavel/ParallelComputationExamples/blob/master/CUDA/03.5-matrix-multiplication-example/).

## How to submit the task

1. Each of you has received access details for http://gitlab.atp-fivt.org/. There you can find a repository named `<your_name>-cuda`.
2. For each task (only one in this particular case) a branch has been created, into which you must push your solution.
3. To submit the solution, you should create a pull request into master branch before the the deadline. The request should contain:
    - main code
    - experiment results: (1) charts, if needed (img, png or pdf), (2) time measurement results
    - code that allows to reproduce your experiments
4. After the deadline, a reviewer (who can be any of the teachers or assistants) will comment it and rate it with *current grade*. If your solutions has any issues, but is otherwise mostly correct and meets the requirements, you can fix it and improve your grade.
